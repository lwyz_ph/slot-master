﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public int startingSeconds = 7;
    public int startingMinutes = 0;
    public bool startOnAwake = false;
    public bool onlySeconds = true;
    public UnityEvent onTimerFinish;

    private bool running = false;
    private float secondsLeft;
    private Text timerText;

    private void Awake()
    {
        if (startOnAwake) { StartTimer(); }
    }

    // Start is called before the first frame update
    void Start()
    {
        timerText = GetComponent<Text>();
        timerText.text = onlySeconds ? $"{startingSeconds}s" :
            $"{startingMinutes:00}:{startingSeconds:00}";
    }

    // Update is called once per frame
    void Update()
    {
        if (!running) { return; }

        secondsLeft -= Time.deltaTime;
        int currentMinutes = Mathf.FloorToInt(secondsLeft / 60f);
        int currentSeconds = Mathf.FloorToInt(secondsLeft % 60f);
        timerText.text = onlySeconds ? $"{currentSeconds}s" :
            $"{currentMinutes:00}:{currentSeconds:00}";
        if (secondsLeft <= 0f)
        {
            timerText.text = onlySeconds ? $"0s" : "00:00";
            running = false;
            onTimerFinish?.Invoke();
        }
    }

    public void StartTimer()
    {
        running = true;
        secondsLeft = startingSeconds + startingMinutes * 60;
    }

    public void PauseTimer()
    {
        running = false;
    }

    public void ResumeTimer()
    {
        running = true;
    }

    public void ResetTimer()
    {
        secondsLeft = startingSeconds + startingMinutes * 60;
        int currentMinutes = Mathf.FloorToInt(secondsLeft / 60f);
        int currentSeconds = Mathf.FloorToInt(secondsLeft % 60f);
        timerText.text = onlySeconds ? $"{currentSeconds}s" :
            $"{currentMinutes:00}:{currentSeconds:00}";
    }
}
