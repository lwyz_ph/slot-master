﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ReelPosition { Left, Middle, Right }

public enum PrizeType
{
    Jackpot,
    Triple,
    Double7,
    DoubleAndOne7,
    Double,
    Seven,
    NONE
}

[System.Serializable]
public struct PrizeData
{
    public PrizeType type;
    public float moneyPrize;
    public string prizeText;
    public string moneyPrizeText;
    public AudioClip[] sfxs;

    public override string ToString()
    {
        return $"Type {type} and it's worth {moneyPrize}";
    }
}

public class GameManager : MonoBehaviour
{
    #region Public Fields
    public const int MAX_LEVEL = 7;
    public PaylineDetector[] paylineDetectors;
    public ReelSpinner[] reelSpinners;
    public float minRandomStopWaitTime = 0f;
    public float maxRandomStopWaitTime = 1f;
    public Timer ticketTimer;
    public int maxTicketsOnTimer = 7;
    public int ticketPrice = 100;
    public PrizeData[] prizeDatas;
    #endregion

    #region Events
    public UnityEvent onAllReelsStopped;

    public static event System.Action OnReelsStartSpinning;
    public static event System.Action<int> OnReelStopped;
    public static event System.Action<float> OnPlayerMoneyModified;
    public static event System.Action<int> OnPlayerTicketsModified;
    public static event System.Action<int> OnPlayerLevelUp;
    public static event System.Action<PrizeData> OnPrizeWon;
    #endregion

    #region Private Fields
    private Player player;
    private bool gameRunning = false;
    private int paylineReports = 0;
    private SlotSymbol.Type[] endCombination = new SlotSymbol.Type[3];
    #endregion

    public static GameManager Instance;
    private void Awake()
    {
        Instance = this;

        #region DEBUG_TEST
#if UNITY_EDITOR
        float testMoney = 500f;
        int testTickets = 7;
        int testLevel = 1;
        player = new Player(testMoney, testTickets, testLevel);
        SaveAndLoadManager.SavePlayer(player);
#endif
        #endregion

        if (SaveAndLoadManager.IsFirstTimeLaunched())
        {
            player = new Player(0, 7);
            if (!SaveAndLoadManager.SavePlayer(player))
            {
                throw new System.ApplicationException("Error while saving player for the first time!!");
            }
        }
        else
        {
            player = SaveAndLoadManager.LoadPlayer();
        }

        ticketTimer.StartTimer();
        if (player.tickets >= maxTicketsOnTimer) { ticketTimer.PauseTimer(); }
        ClearSettings();
    }

    private void Start() //ProgressionManager Start goes first.
    {
        SetPrizeDatasMoneyText();
        ProgressionManager.OnPrizeUpdate += SetPrizeDatasMoneyText;
    }

    private void Update()
    {
        if (!gameRunning) { return; }

        foreach (var reelSpinner in reelSpinners)
        {
            if (reelSpinner.spinning) { return; } //A reel hasn't stopped, so it's not game over yet.
        }
        //The game has finished if it reaches this part of the code.
        gameRunning = false;
        Debug.Log("Check pay table for prices!");
        EnablePaylines(true);
        onAllReelsStopped.Invoke();
    }

    private void OnDestroy()
    {
        onAllReelsStopped.RemoveAllListeners();
        ProgressionManager.OnPrizeUpdate -= SetPrizeDatasMoneyText;
    }

    public void ReportSymbol(ReelPosition reelPosition, SlotSymbol.Type symbolType)
    {
        endCombination[(int)reelPosition] = symbolType;
        ++paylineReports;
        if (paylineReports == 3)
        {
            CheckResults();
        }
    }

    public void OnClickSpin()
    {
        if (player.tickets == 0)
        {
            return;
        }

        ModifyPlayerTickets(-1);
        gameRunning = true;

        ClearSettings();

        foreach (var reelSpinner in reelSpinners)
        {
            reelSpinner.StartSpinning();
        }
        OnReelsStartSpinning?.Invoke();
    }

    public void StopReel(int reelIndex)
    {
        reelSpinners[reelIndex].StopSpinning();
        OnReelStopped?.Invoke(reelIndex);
    }

    public void EnablePaylines(bool enable = true)
    {
        foreach (var payline in paylineDetectors)
        {
            payline.gameObject.SetActive(enable);
        }
    }

    public float GetPlayerMoney() { return player.money; }

    public int GetPlayerTickets() { return player.tickets; }

    public int GetPlayerLevel() { return player.level; }

    public void ModifyPlayerMoney(float amount)
    {
        player.money += amount;
        if (player.money < 0) { player.money = 0; }

        OnPlayerMoneyModified?.Invoke(player.money);
        if (!SaveAndLoadManager.SavePlayer(player)) { Debug.LogError("Couldn't save the player!"); }
    }

    public void ModifyPlayerTickets(int amount)
    {
        player.tickets += amount;

        if (player.tickets >= maxTicketsOnTimer) { ticketTimer.PauseTimer(); }
        else { ticketTimer.ResumeTimer(); }

        OnPlayerTicketsModified?.Invoke(player.tickets);
        if (!SaveAndLoadManager.SavePlayer(player)) { Debug.LogError("Couldn't save the player!"); }
    }

    public void LevelUpPlayer(int levelPrice)
    {
        if (player.level == MAX_LEVEL)
        {
            Debug.Log("The player already has the max level!");
            return;
        }

        ++player.level;
        ModifyPlayerMoney(-levelPrice);
        OnPlayerLevelUp?.Invoke(player.level);
    }

    public void TicketTimerGain()
    {
        ModifyPlayerTickets(1);
        ticketTimer.ResetTimer();
        if (player.tickets < maxTicketsOnTimer)
        {
            ticketTimer.StartTimer();
        }
    }

    public void BuyTicket()
    {
        if (player.money >= ticketPrice)
        {
            ModifyPlayerMoney(-ticketPrice);
            ModifyPlayerTickets(1);
            OnPlayerMoneyModified(player.money);
            OnPlayerTicketsModified(player.tickets);
        }
    }

    public void StopAllReels()
    {
        StartCoroutine(StopAllReelsRandomly());
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

    /// <summary>
    /// This coroutine will ensure a non-deterministic result if the timer just finishes.
    /// </summary>
    /// <returns></returns>
    private IEnumerator StopAllReelsRandomly()
    {
        for (int i = 0; i < 3; i++)
        {
            if (!reelSpinners[i].spinning) { continue; }

            yield return new WaitForSeconds(Random.Range(minRandomStopWaitTime, maxRandomStopWaitTime));
            reelSpinners[i].StopSpinning();
        }
    }

    private void ClearSettings()
    {
        paylineReports = 0;
        EnablePaylines(false);
    }

    private void CheckResults()
    {
        Dictionary<SlotSymbol.Type, int> symbolsFound = new Dictionary<SlotSymbol.Type, int>();
        for (int i = 0; i < 3; i++)
        {
            SlotSymbol.Type currentType = endCombination[i];

            if (symbolsFound.ContainsKey(currentType)) { ++symbolsFound[currentType]; }
            else { symbolsFound.Add(currentType, 1); }
        }
        //TODO: Remove this Debugs.
        foreach (var symbolFound in symbolsFound)
        {
            Debug.Log($"Found {symbolFound.Value} of {symbolFound.Key}");
        }

        switch (symbolsFound.Count)
        {
            case 0: goto default;
            case 1: //It's a triple! We check for the major jackpot or just another triple.
                {
                    foreach (var symbolFound in symbolsFound)
                    {//This foreach is just one iteration, need it to access the one element without adding LINQ.
                        if (symbolFound.Key == SlotSymbol.Type.Lucky7) { PrizeWon(PrizeType.Jackpot); }
                        else if (symbolFound.Key != SlotSymbol.Type.NONE) { PrizeWon(PrizeType.Triple); }
                        else { goto default; } //Triple nothing is not worth anything!
                    }
                    break;
                }
            case 2: //We need to check 3 cases here.
                {
                    int lucky7sFound = 0;
                    int otherSymbolFound = 0;
                    foreach (var symbolFound in symbolsFound)
                    {
                        if (symbolFound.Key == SlotSymbol.Type.Lucky7) { lucky7sFound = symbolFound.Value; }
                        else if (symbolFound.Key != SlotSymbol.Type.NONE
                            && symbolFound.Value > otherSymbolFound) { otherSymbolFound = symbolFound.Value; }
                    }

                    if (lucky7sFound == 2) { PrizeWon(PrizeType.Double7); }
                    else if (otherSymbolFound == 2)
                    {
                        if (lucky7sFound == 1) { PrizeWon(PrizeType.DoubleAndOne7); }
                        else { PrizeWon(PrizeType.Double); }
                    }
                    else if (lucky7sFound == 1) { PrizeWon(PrizeType.Seven); }
                    else { goto default; }
                    break;
                }
            case 3: //All 3 are different, so we just check if one of them is a lucky 7.
                {
                    foreach (var symbolFound in symbolsFound)
                    {
                        if (symbolFound.Key == SlotSymbol.Type.Lucky7)
                        {
                            PrizeWon(PrizeType.Seven);
                            return;
                        }
                    }
                }
                goto default;
            default:
                PrizeWon(PrizeType.NONE);
                return;
        }
    }

    private void PrizeWon(PrizeType prizeType)
    {
        Debug.Log($"Prize won is {prizeDatas[(int)prizeType]}");
        OnPrizeWon?.Invoke(prizeDatas[(int)prizeType]);
        ModifyPlayerMoney(prizeDatas[(int)prizeType].moneyPrize);
    }

    private void SetPrizeDatasMoneyText()
    {
        for (int i = 0; i < prizeDatas.Length - 1; i++) //Skip the last iteration because it's the sorry no price one.
        {
            prizeDatas[i].moneyPrizeText = $"{prizeDatas[i].moneyPrize:0.00}!!";
        }
    }
}
