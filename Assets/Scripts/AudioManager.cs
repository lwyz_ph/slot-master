﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public List<AudioClip> musicAudioClips;
    public AudioClip spinningReels;
    [Tooltip("How many SFX are we allowed to play at the same time?")]
    public int maxSimultaneousSFX = 5;

    private int currentMusicIndex = 0;
    private AudioSource musicAudioSource;
    private AudioSource loopingSFX;
    private List<AudioClip> musicList = new List<AudioClip>();
    private Queue<AudioSource> sfxAudioSourceQ;

    public static AudioManager Instance;
    private void Awake()
    {
        Instance = this;

        int musicListCount = musicAudioClips.Count;
        for (int i = 0; i < musicListCount; i++)
        {
            int randomIndex = Random.Range(0, musicAudioClips.Count);
            musicList.Add(musicAudioClips[randomIndex]);
            musicAudioClips.RemoveAt(randomIndex);
        }

        musicAudioSource = gameObject.AddComponent<AudioSource>();
        musicAudioSource.clip = musicList[currentMusicIndex];
        musicAudioSource.loop = false;
        musicAudioSource.volume /= 1.5f;
        musicAudioSource.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        sfxAudioSourceQ = new Queue<AudioSource>(maxSimultaneousSFX);
        for (int i = 0; i < maxSimultaneousSFX; i++)
        {
            sfxAudioSourceQ.Enqueue(gameObject.AddComponent<AudioSource>());
        }

        GameManager.OnReelsStartSpinning += SpinningReels;
        GameManager.Instance.onAllReelsStopped.AddListener(StopSFXLooping);
        GameManager.OnPrizeWon += PrizeWon;
    }

    private void OnDestroy()
    {
        GameManager.OnReelsStartSpinning -= SpinningReels;
        GameManager.OnPrizeWon -= PrizeWon;
    }

    // Update is called once per frame
    void Update()
    {
        if (!musicAudioSource.isPlaying) //Keep the playlist looping forever.
        {
            ++currentMusicIndex;
            currentMusicIndex %= musicList.Count;
            musicAudioSource.clip = musicList[currentMusicIndex];
            musicAudioSource.Play();
        }
    }

    public void PlaySFX(AudioClip sfx)
    {
        if (sfxAudioSourceQ.Count == 0)
        {
            Debug.Log("The Queue is full! Sorry we can't play that clip!");
            return;
        }

        AudioSource audioSource = sfxAudioSourceQ.Dequeue();
        audioSource.PlayOneShot(sfx);
        StartCoroutine(EnqueueSource(audioSource, sfx.length));
    }

    public void PlaySFXLooping(AudioClip sfx)
    {
        if (sfxAudioSourceQ.Count == 0)
        {
            Debug.Log("The Queue is full! Sorry we can't play that clip!");
            return;
        }

        loopingSFX = sfxAudioSourceQ.Dequeue();
        loopingSFX.clip = sfx;
        loopingSFX.loop = true;
        loopingSFX.Play();
    }

    public void StopSFXLooping()
    {
        if (loopingSFX != null && loopingSFX.isPlaying)
        {
            loopingSFX.loop = false;
            loopingSFX.Stop();
            loopingSFX.clip = null;
            StartCoroutine(EnqueueSource(loopingSFX, 0f));
        }
    }

    public void PrizeWon(PrizeData prizeData)
    {
        foreach (var sfx in prizeData.sfxs)
        {
            PlaySFX(sfx);
        }
    }

    private IEnumerator EnqueueSource(AudioSource audioSource, float time)
    {
        yield return new WaitForSeconds(time);
        sfxAudioSourceQ.Enqueue(audioSource);
    }

    private void SpinningReels()
    {
        PlaySFXLooping(spinningReels);
    }
}
