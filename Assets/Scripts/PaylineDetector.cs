﻿using System.Collections;
using UnityEngine;

public class PaylineDetector : MonoBehaviour
{
    public ReelPosition reelPosition;

    private SlotSymbol.Type symbolFound;
    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
    }

    private void OnEnable()
    {
        symbolFound = SlotSymbol.Type.NONE;
        StartCoroutine(ReportDelayer());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SlotSymbol slotSymbol = (SlotSymbol)collision.GetComponent(typeof(SlotSymbol));
        symbolFound = slotSymbol == null ? SlotSymbol.Type.NONE : slotSymbol.type;
    }

    private IEnumerator ReportDelayer()
    {//Just wait a little bit to see if onTriggerEnter executes.
        yield return new WaitForSeconds(0.1f);
        gameManager.ReportSymbol(reelPosition, symbolFound);
    }
}
